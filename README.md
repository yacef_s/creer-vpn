

## openvpn-install
OpenVPN [road warrior](http://en.wikipedia.org/wiki/Road_warrior_%28computing%29) installer pour Ubuntu, Debian, AlmaLinux, Rocky Linux, CentOS and Fedora.

Ce script vous permettra de configurer votre propre serveur VPN en moins d'une minute, même si vous n'avez jamais utilisé OpenVPN auparavant. Il a été conçu pour être aussi discret et universel que possible.

### Installation
Run le script et suivre l'assistant:

`wget https://git.io/vpn -O openvpn-install.sh && bash openvpn-install.sh`

Une fois terminé, vous pouvez le réexécuter pour ajouter plus d'utilisateurs, en supprimer certains ou même désinstaller complètement OpenVPN.

### je veux lancer mon propre VPN mais je n'ai pas d'autre server pour ça.
tu peux avoir un vps pour 1$/mois sur [VirMach](https://billing.virmach.com/aff.php?aff=4109&url=billing.virmach.com/cart.php?gid=18).

